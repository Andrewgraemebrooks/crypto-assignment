<?php
/**
 * DisplayCryptoDetailsController.php
 *
 * Sessions: PHP web application to demonstrate how databases
 * are accessed securely
 *
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class ShowEditCryptoMachineFormController extends ControllerAbstract
{
    
    public function createHtmlOutput()
    {
        $validatedInput = $this->validate();

        $cryptoMachine = $this->getCryptoMachine($validatedInput);
        var_dump($cryptoMachine);
        if ($cryptoMachine['fk_user_id'] == SessionsWrapper::getSession('user-id')) {
            $view = Factory::buildObject('EditCryptoMachineView');
            $view->cryptoMachine = $cryptoMachine;
            $view->createPage();
        } else {
            $view = Factory::buildObject('DisplayCryptoDetailsView');
            $view->createForm();
        }

        $this->html_output = $view->getHtmlOutput();
    }

    private function validate()
    {
        $cleaned['validated-machine-id'] = intval($_POST['cryptomachineid']);
        return $cleaned;
    }

    public function getCryptoMachine($validatedInput) {
        $database = Factory::createDatabaseWrapper();
        $model = Factory::buildObject('DisplayIndividualCryptoMachineModel');

        $model->setDatabaseHandle($database);

        return $model->getCryptoMachine($validatedInput);
    }
}