<?php
/**
 * ShowCreateCryptoFormController.php
 *
 * Sessions: PHP web application to demonstrate how databases
 * are accessed securely
 *
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class ShowCreateCryptoFormController extends ControllerAbstract
{
    public function createHtmlOutput()
    {
        $logged_in = SessionsWrapper::checkLoggedIn();
        if (!$logged_in) {
            $view = Factory::buildObject('UserLoginFormView');
            $view->createLoginForm();
        } else {
            $view = Factory::buildObject('CreateCryptoFormView');
            $view->createForm();
        }
        $this->html_output = $view->getHtmlOutput();
    }
}