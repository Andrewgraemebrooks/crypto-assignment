<?php
/**
 * CreateCryptoMachineController.php
 *
 * Sessions: PHP web application to demonstrate how databases
 * are accessed securely
 *
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DeleteCryptoMachineController extends ControllerAbstract
{
    public function createHtmlOutput()
    {
        $validated_input = $this->validate();
        $input_error = false;

        if (!$input_error)
        {
            $deleteMachineResult = $this->deleteCryptoMachine($validated_input);
        }

        $this->html_output = $this->createView($deleteMachineResult);
    }

    private function validate()
    {

        $cleaned['validated-machine-id'] = $_POST['cryptomachineid'];

        return $cleaned;
    }

    private function deleteCryptoMachine($validated_input)
    {
        $database = Factory::createDatabaseWrapper();
        $model = Factory::buildObject('DeleteCryptoMachineModel');

        $model->setDatabaseHandle($database);

        $model->setValidatedInput($validated_input);

        $deleteMachineResult = $model->deleteCryptoMachine();

        return $deleteMachineResult;
    }

    private function createView($deleteMachineResult)
    {
        $view = Factory::buildObject('DeleteCryptoMachineView');
        $view->createPage($deleteMachineResult);
        $html_output = $view->getHtmlOutput();

        return $html_output;
    }
}