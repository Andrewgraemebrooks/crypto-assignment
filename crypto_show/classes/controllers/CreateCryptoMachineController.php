<?php
/**
 * CreateCryptoMachineController.php
 *
 * Sessions: PHP web application to demonstrate how databases
 * are accessed securely
 *
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class CreateCryptoMachineController extends ControllerAbstract
{
    public function createHtmlOutput()
    {
        $input_error = true;
        $register_new_user_result = [];

        $validated_input = $this->validate();
        $input_error = false;

        if (!$input_error)
        {
            $register_new_machine_result = $this->registerNewMachine($validated_input);
        }

        $this->html_output = $this->createView();
    }

    private function validate()
    {

        $cleaned['validated-machine-name'] = $_POST['cryptomachinename'];
        $cleaned['validated-machine-model'] = $_POST['cryptomachinemodel'] ?? null;
        $cleaned['validated-machine-desc'] = $_POST['cryptomachinedesc'] ?? null;
        $cleaned['validated-machine-country-of-origin'] = $_POST['cryptomachinecountryoforigin'] ?? null;
        $cleaned['validated-machine-date-of-invention'] = $_POST['cryptomachinedateofinvention'] ?? null;
        $cleaned['validated-crypto-machine-visibility'] = ($_POST['cryptomachinerecordvisible'] == 'on') ? 1 : 0;

        return $cleaned;
    }

    private function registerNewMachine($validated_input)
    {
        $database = Factory::createDatabaseWrapper();
        $model = Factory::buildObject('CreateCryptoMachineModel');

        $model->setDatabaseHandle($database);

        $model->setValidatedInput($validated_input);
        $register_new_machine_result = $model->createCryptoMachine();

        return $register_new_machine_result;
    }

    private function createView()
    {
        $view = Factory::buildObject('DisplayCryptoDetailsView');
        $view->createForm();
        $html_output = $view->getHtmlOutput();

        return $html_output;
    }
}