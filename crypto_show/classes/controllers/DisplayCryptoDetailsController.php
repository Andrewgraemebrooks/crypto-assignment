<?php
/**
 * DisplayCryptoDetailsController.php
 *
 * Sessions: PHP web application to demonstrate how databases
 * are accessed securely
 *
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DisplayCryptoDetailsController extends ControllerAbstract
{
    public function createHtmlOutput()
    {
        $view = Factory::buildObject('DisplayCryptoDetailsView');
        $view->createForm();
        $this->html_output = $view->getHtmlOutput();
    }
}