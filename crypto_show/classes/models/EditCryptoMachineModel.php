<?php
/**
 * EditCryptoMachineModel.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class EditCryptoMachineModel extends ModelAbstract
{

    private $validated_new_machine_details;

    public function __construct()
    {
        parent::__construct();
        $this->validated_new_machine_details = '';
    }

    public function __destruct()
    {
    }

    public function setDatabaseHandle($database_handle)
    {
        $this->database_handle = $database_handle;
    }

    public function setValidatedInput($validated_input)
    {
        $this->validated_new_machine_details = $validated_input;
    }

    public function editCryptoMachine()
    {
        $new_crypto_machine_details_stored = false;


        $sql_query_string = SqlQuery::queryEditCryptoMachine();
        $sql_query_parameters = array(
            ':cryptomachineid' => $this->validated_new_machine_details['validated-machine-id'],
            ':fkuserid' =>  SessionsWrapper::getSession('user-id'),
            ':cryptomachinename' => $this->validated_new_machine_details['validated-machine-name'],
            ':cryptomachinemodel' => $this->validated_new_machine_details['validated-machine-model'],
            ':cryptomachinedesc' => $this->validated_new_machine_details['validated-machine-desc'],
            ':cryptomachinecountryoforigin' => $this->validated_new_machine_details['validated-machine-country-of-origin'],
            ':cryptomachinedateofinvention' => $this->validated_new_machine_details['validated-machine-date-of-invention'],
            ':cryptomachinevisibility' => $this->validated_new_machine_details['validated-crypto-machine-visibility']
        );

        $this->database_handle->safeQuery($sql_query_string, $sql_query_parameters);
        if($this->database_handle->countRows() == 1) {
            $new_crypto_machine_details_stored = true;
        }

        return $new_crypto_machine_details_stored;

    }
}
