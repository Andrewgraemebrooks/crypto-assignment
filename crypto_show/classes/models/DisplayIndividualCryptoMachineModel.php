<?php
/**
 * DisplayIndividualCryptoMachineModel.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DisplayIndividualCryptoMachineModel extends ModelAbstract
{

    protected $validatedInput;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct(){}

    public function setDatabaseHandle($database_handle)
    {
        $this->database_handle = $database_handle;
    }

    public function setValidatedInput($sanitized_input) {
        $this->validatedInput = $sanitized_input;
    }

    public function getCryptoMachine($validatedInput) {
        $getCryptoMachineQuery = SqlQuery::queryGetCryptoMachine();
        $getCryptoMachineParameters = array(
            'machine_id' => $validatedInput['validated-machine-id']
        );

        $this->database_handle->safeQuery($getCryptoMachineQuery, $getCryptoMachineParameters);

        if($this->database_handle->countRows() == 1) {
            return $this->database_handle->safeFetchArray();
        }
        return false;

    }

}
