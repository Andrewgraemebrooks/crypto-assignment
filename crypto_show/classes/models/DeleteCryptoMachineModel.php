<?php
/**
 * CreateCryptoMachineModel.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DeleteCryptoMachineModel extends ModelAbstract
{

    private $validatedInput;

    public function __construct()
    {
        parent::__construct();
        $this->validatedInput = '';
    }

    public function __destruct()
    {
    }

    public function setDatabaseHandle($database_handle)
    {
        $this->database_handle = $database_handle;
    }

    public function setValidatedInput($validated_input)
    {
        $this->validatedInput = $validated_input;
    }

    public function deleteCryptoMachine()
    {
        $deleteMachineResult = false;

        $getCryptoMachineSQL = SqlQuery::queryGetCryptoMachine();
        $getCryptoMachineParam = array(
            'machine_id' => $this->validatedInput['validated-machine-id']
        );

        $this->database_handle->safeQuery($getCryptoMachineSQL, $getCryptoMachineParam);
        $cryptoMachine = $this->database_handle->safeFetchObject();

        if($cryptoMachine->fk_user_id == SessionsWrapper::getSession('user-id')) {
            $deleteMachineQuery = SqlQuery::queryDeleteCryptoMachine();
            $deleteMachineParam = array(
                'cryptomachineid' => $this->validatedInput['validated-machine-id']
            );
            
            $this->database_handle->safeQuery($deleteMachineQuery, $deleteMachineParam);
            if($this->database_handle->countRows() == 1) {
                $deleteMachineResult = true;
            }

            $getUserCryptoMachines = SqlQuery::queryGetUserCryptoMachines();
            $this->database_handle->safeQuery($getUserCryptoMachines, array('fk_user_id' => SessionsWrapper::getSession('user-id')));

            $updateMachineCountSQL = SqlQuery::queryUpdateUserMachineCount();
            $updateMachineCountParam = array(
                'user_id' => SessionsWrapper::getSession('user-id'),
                'numberOfCryptoMachines' => $this->database_handle->countRows()
            );

            $this->database_handle->safeQuery($updateMachineCountSQL, $updateMachineCountParam);
        }

        return $deleteMachineResult;

    }
}
