<?php
/**
 * CreateCryptoMachineModel.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class CreateCryptoMachineModel extends ModelAbstract
{

    private $store_new_machine_details_result;
    private $validated_new_machine_details;

    public function __construct()
    {
        parent::__construct();
        $this->store_new_machine_details_result = [];
        $this->validated_new_machine_details = '';
    }

    public function __destruct()
    {
    }

    public function setDatabaseHandle($database_handle)
    {
        $this->database_handle = $database_handle;
    }

    public function setValidatedInput($validated_input)
    {
        $this->validated_new_machine_details = $validated_input;
    }

    public function createCryptoMachine()
    {
        $new_crypto_machine_details_stored = false;

        $numberOfCryptoMachinesSQL = SqlQuery::queryGetUserCryptoMachines();
        $numberOfCryptoMachinesParamaters = array(
            ':fk_user_id' => SessionsWrapper::getSession('user-id')
        );

        $nOCMResult = $this->database_handle->safeQuery($numberOfCryptoMachinesSQL, $numberOfCryptoMachinesParamaters);
        $numberOfCryptoMachines = $this->database_handle->countRows();

        if($numberOfCryptoMachines <= 9) {
            $sql_query_string = SqlQuery::queryCreateCryptoMachine();
            $sql_query_parameters = array(
                ':fkuserid' =>  SessionsWrapper::getSession('user-id'),
                ':cryptomachinename' => $this->validated_new_machine_details['validated-machine-name'],
                ':cryptomachinemodel' => $this->validated_new_machine_details['validated-machine-model'],
                ':cryptomachinedesc' => $this->validated_new_machine_details['validated-machine-desc'],
                ':cryptomachinecountryoforigin' => $this->validated_new_machine_details['validated-machine-country-of-origin'],
                ':cryptomachinedateofinvention' => $this->validated_new_machine_details['validated-machine-date-of-invention'],
                ':cryptomachinevisibility' => $this->validated_new_machine_details['validated-crypto-machine-visibility']
            );
    
            $query_result = $this->database_handle->safeQuery($sql_query_string, $sql_query_parameters);
    
            $rows_inserted = $this->database_handle->countRows();
    
            if ($rows_inserted == 1) {
                $new_crypto_machine_details_stored = true;
            }

            if ($new_crypto_machine_details_stored) {
                $updateUserCryptoMachineCountSQL = SqlQuery::queryUpdateUserMachineCount();
                $updateUserCountParameters = array(
                    'user_id' => SessionsWrapper::getSession('user-id'),
                    'numberOfCryptoMachines' => $numberOfCryptoMachines + 1
                );
                $this->database_handle->safeQuery($updateUserCryptoMachineCountSQL, $updateUserCountParameters);
            }
        }

        return $new_crypto_machine_details_stored;

    }
}
