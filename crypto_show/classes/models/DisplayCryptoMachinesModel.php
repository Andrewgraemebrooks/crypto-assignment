<?php
/**
 * UserLoginProcessModel.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DisplayCryptoMachinesModel extends ModelAbstract
{

    protected $validatedInput;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct(){}

    public function setDatabaseHandle($database_handle)
    {
        $this->database_handle = $database_handle;
    }

    public function setValidatedInput($sanitized_input) {
        $this->validatedInput = $sanitized_input;
    }

    public function getAllCryptoMachines() {
        $sql_query_string = SqlQuery::queryGetAllCryptoMachines();
        $result = $this->database_handle->safeQuery($sql_query_string, array(':fk_user_id' => SessionsWrapper::getSession('user-id')));
        if($result['execute-OK']) {
            $result = $this->database_handle->safeFetchAllResults();
        }
        return $result;
    }

}
