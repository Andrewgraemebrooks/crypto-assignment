<?php
/**
 * DeleteCryptoMachineView.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DeleteCryptoMachineView extends WebPageTemplateView
{
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct(){}

    public function createPage($deleteMachineResult)
    {
        $this->setPageTitle();
        $this->createPageBody($deleteMachineResult);
        $this->createWebPage();
    }

    public function getHtmlOutput()
    {
        return $this->html_page_output;
    }

    private function setPageTitle()
    {
        $this->page_title = APP_NAME . ' Machine Deleted';
    }

    private function createPageBody($deleteMachineResult)
    {
        $year = date('Y');
        $info_text = '';
        $info_text .= 'Welcome to the Cryptographic Machine Show web-site ' . $year;
        $info_text .= '<br />';

        if($deleteMachineResult) {
            $page_content = "<h2>Crypto machine deleted.</h2>";
        } else {
            $page_content = "<h2>Could not delete crypto machine</h2>";
        }


        $page_heading = APP_NAME . ' demonstration';

        $this->html_page_content = <<< HTMLFORM
<h2>$page_heading</h2>
<p>$info_text</p>
$page_content
HTMLFORM;
    }
}
