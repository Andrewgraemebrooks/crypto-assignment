<?php
/**
 * ShowCreateCryptoFormView.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class CreateCryptoFormView extends WebPageTemplateView
{

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct(){}

    public function createForm()
    {
        $this->setPageTitle();
        $this->createPageBody();
        $this->createWebPage();
    }

    public function getHtmlOutput()
    {
        return $this->html_page_output;
    }

    private function setPageTitle()
    {
        $this->page_title = APP_NAME . ' Display Crypto Details View';
    }



    private function createPageBody()
    {
        $year = date('Y');
        $info_text = '';
        $info_text .= 'Welcome to the Cryptographic Machine Show web-site ' . $year;
        $info_text .= '<br />';
        $info_text .= 'Please select a crypto machine from the dropdown to learn more about it!';
        $info_text .= '<br />';

        $page_heading = APP_NAME . ' Display Crypto Details';

        $form = "
            <input type='text' name='feature' value='create_crypto_machine' hidden>
            <label for='cryptomachinename'>Crypto Machine Name</label>
            <input name='cryptomachinename' type='text' required>
            <br>
            <label for='cryptomachinemodel'>Crypto Machine Model</label>
            <input name='cryptomachinemodel' type='text' >
            <br>
            <label for='cryptomachinedesc'>Crypto Machine Description</label>
            <textarea name='cryptomachinedesc'></textarea>
            <br>
            <label for='cryptomachinecountryoforigin'>Crypto Machine Country of Origin</label>
            <input name='cryptomachinecountryoforigin' type='text' >
            <br>
            <label for='cryptomachinedateofinvention'>Crypto Machine Date of Invention</label>
            <input name='cryptomachinedateofinvention' type='date' >
            <br>
            <label>Crypto Machine Record Visible</label>
            <select name='cryptomachinerecordvisible'>
                <option value='on'>Registered</option>
                <option value='off'>Private</option>
            </select>
            <br>
            <button type='submit'>Create</button>
        ";

        $this->html_page_content = <<< HTMLFORM
<h2>$page_heading</h2>
<p>$info_text</p>
<form method='post' action='http://localhost/'>$form</form>
HTMLFORM;
    }
}
