<?php
/**
 * DisplayCryptoDetailsView.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DisplayCryptoDetailsView extends WebPageTemplateView
{

    private $allCryptoMachines;

    public function __construct()
    {
        parent::__construct();
        $this->allCryptoMachines = '';
    }

    public function __destruct(){}

    public function createForm()
    {
        $this->getCryptoMachines();
        $this->setPageTitle();
        $this->createPageBody();
        $this->createWebPage();
    }

    public function getHtmlOutput()
    {
        return $this->html_page_output;
    }

    private function setPageTitle()
    {
        $this->page_title = APP_NAME . ' Display Crypto Details View';
    }

    private function getCryptoMachines() {
        $database = Factory::createDatabaseWrapper();
        $model = Factory::buildObject('DisplayCryptoMachinesModel');

        $model->setDatabaseHandle($database);
        $this->allCryptoMachines = $model->getAllCryptoMachines();
    }

    private function createPageBody()
    {
        $year = date('Y');
        $info_text = '';
        $info_text .= 'Welcome to the Cryptographic Machine Show web-site ' . $year;
        $info_text .= '<br />';
        $info_text .= 'Please select a crypto machine from the dropdown to learn more about it!';
        $info_text .= '<br />';

        $page_heading = APP_NAME . ' Display Crypto Details';

        $select_form = "<select name='crypto_machine_id_to_view'>";

        foreach($this->allCryptoMachines as $machine) {
            $select_form .= "<option value='".$machine['crypto_machine_id']."'>".$machine['crypto_machine_name']."</option>";
        }

        $select_form .= "
        </select>
        <br>
        <input type='text' name='feature' value='display_individual_crypto_machine' hidden>
        <button type='submit'>Submit</button>";

        $this->html_page_content = <<< HTMLFORM
<h2>$page_heading</h2>
<p>$info_text</p>
<form method='post' action='http://localhost/'>$select_form</form>
HTMLFORM;
    }
}
