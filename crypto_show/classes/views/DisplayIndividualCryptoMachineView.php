<?php
/**
 * DisplayIndividualCryptoMachineView.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class DisplayIndividualCryptoMachineView extends WebPageTemplateView
{

    public $cryptoMachine;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct(){}

    public function createPage()
    {
        $this->setPageTitle();
        $this->createPageBody();
        $this->createWebPage();
    }

    public function getHtmlOutput()
    {
        return $this->html_page_output;
    }

    private function setPageTitle()
    {
        $this->page_title = APP_NAME . ' Display Crypto Details View';
    }


    private function createPageBody()
    {
        $year = date('Y');
        $info_text = '';
        $info_text .= 'Welcome to the Cryptographic Machine Show web-site ' . $year;
        $info_text .= '<br />';
        $info_text .= '<h1>'.$this->cryptoMachine['crypto_machine_name'].'</h1>';
        $info_text .= '<br />';

        $page_heading = APP_NAME . ' Display Individual Crypto Details';

        $page_content = "<p>Name: ".$this->cryptoMachine['crypto_machine_name']."</p>";
        $page_content .= "<p>Model: ".$this->cryptoMachine['crypto_machine_model']."</p>";
        $page_content .= "<p>Description: ".$this->cryptoMachine['crypto_machine_desc']."</p>";
        $page_content .= "<p>Country of origin: ".$this->cryptoMachine['crypto_machine_country_of_origin']."</p>";
        $page_content .= "<p>Date of invention: ".$this->cryptoMachine['crypto_machine_date_of_invention']."</p>";
        $page_content .= "<p>Image: ".$this->cryptoMachine['crypto_machine_image_name']."</p><br>";

        $formToEditOrDelete = "
        <form method='post' action='http://localhost/'>
            <input type='hidden' name='cryptomachineid' value='".$this->cryptoMachine['crypto_machine_id']."' hidden>
            <button name='feature' value='show-edit-crypto-machine-form'>Edit</button>
            <button name='feature' value='delete-crypto-machine'>Delete</button>
        </form>
        ";

        if ($this->cryptoMachine['fk_user_id'] == SessionsWrapper::getSession('user-id')) {
            $page_content.=$formToEditOrDelete;
        }

        $this->html_page_content = <<< HTMLFORM
<h2>$page_heading</h2>
<p>$info_text</p>
$page_content
HTMLFORM;
    }
}
