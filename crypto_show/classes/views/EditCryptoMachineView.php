<?php
/**
 * DisplayIndividualCryptoMachineView.php
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package crypto-show
 */

class EditCryptoMachineView extends WebPageTemplateView
{

    public $cryptoMachine;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct(){}

    public function createPage()
    {
        $this->setPageTitle();
        $this->createPageBody();
        $this->createWebPage();
    }

    public function getHtmlOutput()
    {
        return $this->html_page_output;
    }

    private function setPageTitle()
    {
        $this->page_title = APP_NAME . ' Display Crypto Details View';
    }


    private function createPageBody()
    {
        $year = date('Y');
        $info_text = '';
        $info_text .= 'Welcome to the Cryptographic Machine Show web-site ' . $year;
        $info_text .= '<br />';
        $info_text .= '<h1>'.$this->cryptoMachine['crypto_machine_name'].'</h1>';
        $info_text .= '<br />';

        $page_heading = APP_NAME . ' Edit Crypto Machine Details';

        $page_content = "
            <form method='post' action='http://localhost/'>
                <input type='text' name='feature' value='edit-crypto-machine' hidden>
                <input type='number' name='cryptomachineid' value='".$this->cryptoMachine['crypto_machine_id']."' hidden>
                ";
        $page_content .= "
                <label for='cryptomachinename'>Crypto Machine Name</label>
                <input name='cryptomachinename' type='text' value='".$this->cryptoMachine['crypto_machine_name']."' required>
                <br>";
        $page_content .= "
                <label for='cryptomachinemodel'>Crypto Machine Model</label>
                <input name='cryptomachinemodel' type='text' value='".$this->cryptoMachine['crypto_machine_model']."' >
                <br>";
        $page_content .= "
                <label for='cryptomachinedesc'>Crypto Machine Description</label>
                <textarea name='cryptomachinedesc'>".$this->cryptoMachine['crypto_machine_desc']."</textarea>
                <br>";
        $page_content .= "
                <label for='cryptomachinecountryoforigin'>Crypto Machine Country of Origin</label>
                <input name='cryptomachinecountryoforigin' type='text' value='".$this->cryptoMachine['crypto_machine_country_of_origin']."' >
                <br>";
        $page_content .= "
                <label for='cryptomachinedateofinvention'>Crypto Machine Date of Invention</label>
                <input name='cryptomachinedateofinvention' type='date' value='".$this->cryptoMachine['crypto_machine_date_of_invention']."' >
                <br>";
        $page_content .= "
                <label>Crypto Machine Record Visible</label>
                <select name='cryptomachinerecordvisible'>
                    <option value='on' ". (($this->cryptoMachine['crypto_machine_record_visible']) ? ' selected' : '') .">Registered</option>
                    <option value='off' ". (($this->cryptoMachine['crypto_machine_record_visible']) ? '' : ' selected') .">Private</option>
                </select>
                <br>";
        $page_content .= "
                <button type='submit'>Update</button>
            </form>
        ";



        $this->html_page_content = <<< HTMLFORM
<h2>$page_heading</h2>
<p>$info_text</p>
<div>$page_content</div>
HTMLFORM;
    }
}
